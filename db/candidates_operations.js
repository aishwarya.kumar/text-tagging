const candidates = require("./index").db("poc").collection("candidates");
const ObjectId = require("mongodb").ObjectId;
const { text_tagging } = require("../utils/hr_flow_text_tagging");

const getAll = async () => {
  const cursor = await candidates.find({});
  //Converts the result into an array
  return cursor.toArray();
};

const getById = async (id) => {
  return await candidates.findOne({ _id: ObjectId(id) });
};

const removeById = async (id) => {
  await candidates.deleteOne({ _id: ObjectId(id) });
};

const getEligibleCandidates = async (user) => {
  try {
    let text = user.job_title + " - " + user.job_description;
    let jobTags = await text_tagging(text);
    jobTags = JSON.parse(jobTags);
    jobTags = JSON.stringify(jobTags.data.tags);
    //console.log("jobTags:::::::::::::::::::::::::::::",jobTags,typeof(jobTags))

    let eligibleCandidates = await eligibleCandidatesForJob(user);
    //console.log("eligibleCandidateseligibleCandidateseligibleCandidateseligibleCandidates",eligibleCandidates);

    let candidates_below_threshold = [];
    let matched_candidates = [];
    let request_cv_candidates = [];

    for (let user of eligibleCandidates) {
       //console.log("user.work_experience,user.work_experienceuser.work_experience",user.work_experience);
      if (isWorkExperienceExists(user.work_experience)) {
        //console.log("user.work_experience::::::::::::::::::::::::::::",user.work_experience);

        let matchedPercentageJobs = [];
        let sum_percentage = 0;
        let workExpAndPercentage = [];
        let workExpAndLengthOfDescription = [];

        //check if all work experience contains tags in DB
        for (let element of user.work_experience) {
          workExpAndLengthOfDescription.push({
            work_experience_id: element.id,
            LengthOfDescription: element.description.length,
          });
          var tags = element.tags;
          if (jobTags && tags) {
            let emp_tags = jobTags;
            let matchedPercentage = await calculatePercentage(
              JSON.parse(emp_tags),
              tags
            );
            console.log(
              "matchedPercentage::::::::::::::::::::::::::",
              matchedPercentage
            );
            if (matchedPercentage >= 10)
              sum_percentage = sum_percentage + matchedPercentage;
            workExpAndPercentage.push({
              work_experience_id: element.id,
              percentage: matchedPercentage,
            });
            matchedPercentageJobs.push(matchedPercentage);
          }
        }

        let max_percentage = Math.max.apply(Math, matchedPercentageJobs);
        let is_matched = 0;
        if (sum_percentage >= 10 && max_percentage) {
          max_percentage = sum_percentage;
          is_matched = 1;
        }

        if (max_percentage < 10) {
          //candiates not recommended
          candidates_below_threshold.push(user.user_id);
        }

        if (max_percentage >= 10) {
          let requestCVUpdateFlag = 0;
          let machedMoengageEventFlag = 0;
          let workExpAndPercentageArray = workExpAndPercentage;
          let workExpAndLengthOfDescriptionArray =
            workExpAndLengthOfDescription;
          // console.log("workExpAndPercentageArray:::::::::workExpAndLengthOfDescriptionArray",workExpAndPercentageArray,workExpAndLengthOfDescriptionArray);
          for (let elementt of workExpAndLengthOfDescriptionArray) {
            for (let elem of workExpAndPercentageArray) {
              if (
                elementt.work_experience_id == elem.work_experience_id &&
                elementt.LengthOfDescription >= 75 &&
                parseInt(elem.percentage) >= 10
              ) {
                //  console.log("recommendCandidate:::::::::::::::::::",user);
                machedMoengageEventFlag++;
                requestCVUpdateFlag++;
              }
            }
          }
          let data = { user_id: user.user_id };
          if (requestCVUpdateFlag == 0) {
            request_cv_candidates.push(data.user_id);
            // await Admin.getInstance().sendMoengageEventForRequestCvUpdateForNewJob(data);
          }
          if (machedMoengageEventFlag >= 1) {
            matched_candidates.push(data.user_id);
            //  await Admin.getInstance().sendMoengageEventForMatchedCandidatesForNewJob(data);
          }
        }
      } else {
        candidates_below_threshold.push(user.user_id);
      }
    }
    console.log(
      matched_candidates.length,
      " matched_candidates--------------------->"
    );
    console.log(
      request_cv_candidates.length,
      " request_cv_candidates--------------------->"
    );
    console.log(
      candidates_below_threshold.length,
      "candidates_below_threshold------------------------->"
    );

    function calculatePercentage(employerJobTags, candidateWorkExperienceTags) {
      const count = employerJobTags.reduce((acc, val) => {
        if (candidateWorkExperienceTags.includes(val)) {
          return ++acc;
        }
        return acc;
      }, 0);
      return ((count / employerJobTags.length) * 100).toFixed(2);
    }

    function isWorkExperienceExists(checkWorkExperienceExists) {
      return checkWorkExperienceExists[0].id != "NULL";
    }

    return {
      matched_candidates_length: matched_candidates.length,
      request_cv_candidates_length: request_cv_candidates.length,
      candidates_below_threshold_length: candidates_below_threshold.length
    };
  } catch (e) {
    console.error("error_in_getEligibleCandidates ---", e);
    throw e;
  }
};

const eligibleCandidatesForJob = async (eligible_criteria) => {
  let obj = {};
  if (eligible_criteria.gender && eligible_criteria.gender != 0) {
    obj.gender = eligible_criteria.gender;
  }
  if (eligible_criteria.nationality && eligible_criteria.nationality != 0) {
    obj.nationality = { $in: [eligible_criteria.nationality] };
  }

  if (
    eligible_criteria.min_experience &&
    eligible_criteria.min_experience != 0
  ) {
    obj.experience_id = { $gte: eligible_criteria.min_experience };
  }

  if (
    eligible_criteria.min_education_level &&
    eligible_criteria.min_education_level != 0
  ) {
    obj.max_education = { $gte: eligible_criteria.min_education_level };
  }

  if (eligible_criteria.min_age && eligible_criteria.min_age != 0) {
    obj.min_age = { $gte: eligible_criteria.min_age };
  }

  let candidatesDocuments = await candidates
    .find(obj)
    .project({ user_id: 1, _id: 0, work_experience: 1 })
    .toArray();

  return candidatesDocuments;

};

module.exports = { getAll, getById, removeById, getEligibleCandidates };
