const csv = require('csv-parser'); // CSV Parser
const _ = require('lodash'); // Lodash
const fs = require('fs'); // Filesystem
const { MongoClient } = require('mongodb'); // MongoDB Client

// Connection URL
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url,{
  // auth:{
  //   username: 'admin',
  //   password: 'EDU@!%&NDFGCV#MBOL'
  // },
      useNewUrlParser: true,
      useUnifiedTopology: true
  });
const dbName = 'poc';
const collectionName = 'candidates';
//Parse CSV File into array with objects
const parseCSV = async (filename) => {
  const records = [];
  return new Promise((resolve, reject) => {
    fs.createReadStream(filename)
      .on('error', (err) => {
        reject(err);
      })
      .pipe(csv())
      .on('data', (data) => records.push(data))
      .on('end', () => {
        resolve(records);
      });
  });
};

// Map candidates based on user id
const map = async () => {
  try {
    // Client Accounts Parse
    let candidates2 = [];
    const candidates = await parseCSV('./candidates.csv');
    // console.log(candidates[11]);
    for(data of candidates){
      let obj = {};
      let tags = [];
      let keys = Object.keys(data)
      let values = Object.values(data)
      for(let i=0;i<keys.length;i++){

       if (i<11) obj[keys[i]] = values[i]
       else {
        let str = values[i]
        str = str.split('"').join('').split("[").join('').split(',').join('').split(']').join('').trim();
        tags.push(str);
       }
      }
      obj['tags'] = tags
      candidates2.push(obj)
    }
    // Rearrange based on user_id
    const grouping = _(candidates2)
      .groupBy('user_id')
      .map((group, userId) => ({
        user_id: userId,
        first_name: group[0].first_name,
        last_name: group[0].last_name,
        age: group[0].age,
        experience_id: group[0].experience_id,
        gender: group[0].gender,
        nationality: group[0].nationality,
        user_type: group[0].user_type,
        max_education: group[0].course_id,
        work_experience: group.map((data) => ({
          id:data.work_experience_id,
          description: data.description,
          tags: data.tags
        })),
      }))
      .value();

     // console.log("groupinggroupinggroupinggroupinggroupinggroupinggrouping",grouping);

    // Insert into MongoDB
    client.connect(err => {
  if(err){
      console.error(err);
      process.exit(-1);
  }
  console.log("Successfully connected to MongoDB");
})
    const db = client.db(dbName);
    const collection = db.collection(collectionName);

    const result = await collection.insertMany(grouping);
    console.log(result);
    console.log(`${result.insertedCount} documents were inserted`);
    await client.close();
  } catch (e) {
    console.log(e);
  }
};
module.exports = map();

