const jobs = require('./index').db('poc').collection('jobs');
const ObjectId = require('mongodb').ObjectId;



const getAll = async () =>{
    const cursor = await jobs.find();
    //Converts the result into an array
    return cursor.toArray();
}


const getById = async (id) =>{
    return await jobs.findOne({_id:ObjectId(id)});
}


const removeById = async id =>{
    await  jobs.deleteOne({_id:ObjectId(id)});
}


module.exports = {getAll,getById,removeById};