require('dotenv').config();
const koa = require('koa');
const bodyParser = require('koa-bodyparser');
const app = new koa();

//Importing the routes
const candidateRoutes = require('./routes/candidate.route');

//Using body parser
app.use(bodyParser());
 
//Registering the routes
app.use(candidateRoutes.routes()).use(candidateRoutes.allowedMethods());

//Insert candidates array in db from csv
 // const client = require('./db/insert_candidates');


let port = process.env.PORT

app.listen(port,() =>{
    console.log(`Application is listening on port ${port}`)
})

 


