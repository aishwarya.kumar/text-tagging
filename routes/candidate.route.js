const Router = require("@koa/router");
//Import methods created in candidates.api file
const { getCandidates,getCandidate,deleteCandidate} =  require('../api/candidates.api');
const { getJobs, getJob, deleteJob, getEligible } = require("../api/jobs.api");
 
//Define the prefix of the api
const router = new Router({
    prefix:'/candidates'
})
//GET request
router.get('/',async ctx=>{
    ctx.body= await getCandidates() ;
})
//POST request
// router.post('/',async ctx=>{
//     let candidate = ctx.request.body;
//     candidate = await createCandidate(candidate);
//     ctx.response.status = 200;
//     ctx.body = candidate;
// })


router.post("/new_job", async (ctx) => {
    let user = ctx.request.body;
    // payload {job_description,job_title,gender,nationality,min_experience,min_education_level,min_age,max_age}
  
    // text tagging logic here
    let text_tagging = await getEligible(user);
  
    ctx.response.status = 200;
    ctx.body = text_tagging;
  });


router.get('/:id',async ctx=>{
    const id = ctx.params.id;
    ctx.body = await getCandidate(id);
})
//Delete Request
router.delete('/:id',async ctx=>{
    //Get the id from the url
    const id = ctx.params.id;
    await deleteCandidate(id);
})

// router.put('/:id',async ctx=>{
//     const id = ctx.params.id;
//     let candidate = ctx.request.body;
//     candidate= await updateCandidate(id,candidate);
//     ctx.response.status = 200;
//     ctx.body = candidate;
 
// })
//Export the router
module.exports = router;