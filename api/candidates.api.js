const {getAll, getById, removeById} = require('../db/candidates_operations');


//Map the getAll() method
const getCandidates = async ()=>{
    //
    return await getAll();
}
//Map the getById() method
const getCandidate = async id =>{
    return await getById(id);
}
//Map the removeById() method
const deleteCandidate = async id =>{
    return await removeById(id);
}
//update() method
// const updateCandidate = async (id,{})=>{
//     return await update(id,{});
// }
//Export the methods to be used in routes
module.exports = {
    getCandidates,
    getCandidate,
    deleteCandidate
}