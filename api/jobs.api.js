const {getAll, getById, removeById} = require('../db/jobs_operations');
const { getEligibleCandidates } = require('../db/candidates_operations');



//Map the getAll() method
const getJobs = async ()=>{
    //
    return await getAll();
}
//Map the getById() method
const getJob = async id =>{
    return await getById(id);
}
//Map the removeById() method
const deleteJob = async id =>{
    return await removeById(id);
}

//
const getEligible = async user =>{
    return await getEligibleCandidates(user)
}

//update() method
// const updateCandidate = async (id,{})=>{
//     return await update(id,{});
// }
//Export the methods to be used in routes
module.exports = {
    getJobs,
    getJob,
    deleteJob,
    getEligible
}